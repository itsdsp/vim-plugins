function! SwapParams()
	execute 'norm! "9yi('  
	"copies in to 9th numbered register
	let args=@9    
	"Stores that in to args variable
	" echo l:args
	let commaPresent = Contains(',',l:args)
	" echo l:commaPresent
	if(commaPresent == 1) "If commas are present in the string that will be used to switch them
		let splitArgs = split(args,',')
		if len(splitArgs) == 2
			let reassmebledString = StripSpaces(splitArgs[1]) . ", " . StripSpaces(splitArgs[0])
			let @9 = l:reassmebledString
			" echo l:reassmebledString
			execute 'norm! di('
			execute 'norm! "9P'
		endif
	else
		let splitArgs = split(args,' ')
		if len(splitArgs) == 2
			let reassmebledString = StripSpaces(splitArgs[1]) . " " . StripSpaces(splitArgs[0])
			let @9 = l:reassmebledString
			" echo l:reassmebledString
			execute 'norm! di('
			execute 'norm! "9P'
		endif
	endif
endfunction

function! Contains(ch,str)
	let i=0
	while l:i<len(a:str)
		if(a:str[i] == a:ch)
			return 1
		endif
		let l:i += 1
	endwhile
	return 0
endfunction

function! StripSpaces(str)
	return substitute(a:str, '^\s*\(.\{-}\)\s*$', '\1','')
endfunction
