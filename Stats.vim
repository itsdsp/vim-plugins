function! GetStats()
	let wordsList = {}
	let line = getline('.')
	let wordArray = split(line," ")
	for word in wordArray
		if has_key(wordsList,word)
			wordsList[word]++
		else
			let wordsList[word] = 1
		endif
	endfor
	echo l:wordsList
	" echo l:words
	" echo l:line
endfunction
