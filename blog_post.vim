"Delete the header tag on the first line
execute ':1g/^<h/d'
%s/<a href="/<a class="bodyclass" href="/g
:nohlsearch
" redir @a
" %s/<a href="[^c]//gn
" redir END

" "Here '@a' output will be like following if the pattern matches
" " 6 matches on 6 lines
" " 6 matches on 6 lines
" " And if it doesn't match
" " E486: Pattern not found ......

" let d = split(@a,"\n")
" if (len(d) == 2) "Do nothing when length is anything else b/c that would be an error
" 	d = d[0] "Taking the first line as both the lines have the same output
" 	d = split(d)[0] "Here d will have the exact number of matches
" 	let str1 = "execute "
" 	let str2 = str1 . "'normal! Wiclass='"
" 	let str3 = str2 . '"bodyclass"'
" 	let str = d . str3
" endif

" " let len = len(split(@a,"\n"))
" let d = split(@a,"\n")[0]  "Sample output - 6 matches on 6 lines
